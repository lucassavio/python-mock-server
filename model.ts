interface Product {
    code: number;
    producer: { id: number, name: string };
    vendor: { id: number, name: string };
    department: { id: number, name: string };
    category: { id: number, name: string };
    values: { unit: number, modified: number, taxes: number, delivery: number };
    name: string;
    description: string;
    licensed: boolean;
    type: string;
    photos: {p: URL, m: URL, g: URL};
    pictures: { pp: URL, p: URL, m: URL, g: URL, gg: URL};
    dimensions?: { height: number, width: number, depth: number, weight: number};
    delivery?: { value: number, tax: number };
    stock: { control: boolean, quantity?: number};
    info?: [];
}