# PYTHON MOCK SERVER

To run the server, do the following: 

``` bash
$ python3 simple_cors_server.py
```

Make sure that port 8003 is not bein used, or change the `<your_port>` in `simple_cors_server.py` code to any free port

``` python
#!/usr/bin/env python3
# encoding: utf-8

from http.server import HTTPServer, SimpleHTTPRequestHandler

class CORSRequestHandler(SimpleHTTPRequestHandler):
    def end_headers(self):
        self.send_header('Access-Control-Allow-Origin','*')
        self.send_header('Access-Control-Allow-Methods', 'GET')
        self.send_header('Cache-Control', 'no-store, no-cache, must-revalidate')
        return super(CORSRequestHandler, self).end_headers()

httpd = HTTPServer(('localhost', <your_port>), CORSRequestHandler)
httpd.serve_forever()

```
